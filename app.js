var AppRoot = React.createClass({
  displayName: "AppRoot",

  getInitialState: function () {
    return {
      message: 'Loading...',
      page: 0,
      page_count: 0,
      results: []
    }
  },

  componentDidMount: function () {
    this.update()
  },

  hasMorePages: function () {
    return this.state.page < this.state.page_count - 1
  },

  nextPage: function () {
    this.setState({ page: this.state.page + 1 }, this.update)
  },

  setFilter: function (value, filter) {
    this.setState({ filter: filter, value: value, page: 0 }, this.update)
  },

  reset: function() {
    if (this.state.page === 0) {
      this.setState({
        message: 'Loading...',
        results: []
      })
    }
  },

  update: function () {
    this.reset()

    var query = '?page=' + this.state.page

    if (this.state.filter) {
      query += '&' + this.state.filter + '=' + encodeURIComponent(this.state.value)
    }

    console.log(query)

    $.ajax({
      url: this.props.apiBaseUrl + query
    }).done(function(response){
      this.setState({
        page: response.page,
        page_count: response.page_count,
        results: this.state.results.concat(response.results)
      })
      if (response.page_count === 0) {
        this.setState({ message: 'Found no jobs' })
      }
    }.bind(this))
  },

  render: function () {
    return React.createElement("div", {},

      Object.keys(this.props.filters).map(function(filter){
        return React.createElement(Filter, {
          key: filter,
          label: filter,
          filters: this.props.filters[filter],
          setFilter: this.setFilter
        })
      }.bind(this)),

      React.createElement("p", { className: 'page' },
        'Page ' + (this.state.page + 1), ' of ' + this.state.page_count
      ),

      this.state.results.length
        ? this.state.results.map(function(result) {
          return React.createElement(Result, {
            key: result.id,
            result: result
          })
        })
        : React.createElement('p', { className: 'message' }, this.state.message),

      this.hasMorePages() ? React.createElement('footer', null,
        React.createElement('button', { onClick: this.nextPage }, 'Load More')
      ) : null
    )
  }
})

## What is this repository for? ##

[Spec](https://bitbucket.org/levtatz/the-muse/raw/653007b864f93a6b956c1f632de3b672534b70dd/Full-StackEngineerInterviewSpec.pdf)

## How do I get set up? ##

1. [Download](https://bitbucket.org/levtatz/the-muse/get/45ba7a0171b8.zip) or clone the repository
2. Open `index.html` in your browser
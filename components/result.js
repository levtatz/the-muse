var Result = React.createClass({
  displayName: "Result",

  render: function render() {
    var result = this.props.result

    return React.createElement("a", {
      className: 'result',
      href: result.refs.landing_page,
      key: result.id,
      target: '_blank'
    },
      React.createElement('span', { className: 'name' }, result.name),
      React.createElement('span', { className: 'company' }, result.company.name),

      React.createElement('p', {},
        result.categories.map(function(category) {
          return React.createElement('span', { key: category.name }, category.name)
        })
      ),

      React.createElement('p', {},
        result.levels.map(function(level) {
          return React.createElement('span', { key: level.short_name }, level.name)
        })
      ),

      React.createElement('p', {},
        result.locations.map(function(location) {
          return React.createElement('span', { key: location.name }, location.name)
        })
      )
    )
  }
})

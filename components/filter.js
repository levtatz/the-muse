var Filter = React.createClass({
  displayName: "Filter",

  setFilter: function(event) {
    this.props.setFilter(event.target.value, this.props.label)
  },

  render: function render() {
    return React.createElement("div", { className: 'filter' },

      React.createElement('span', {}, this.props.label + ':'),

      React.createElement('div', { className: 'filters' },
        this.props.filters.map(function(filter) {
          return React.createElement('button', {
            onClick: this.setFilter,
            value: filter
          }, filter)
        }.bind(this))
      )

    )
  }
})
